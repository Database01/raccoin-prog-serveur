<?php
namespace controller;

use exception\AuthException;
use model\Annonce;
use model\Membre;
use view\Connexion;
use view\EditerMotDePasse;
use view\EditerProfil;
use view\GenererAPI;
use view\Inscription;
use view\ListeAnnonces;
use view\Profil;

class MembreController extends BaseController {

    public function profil(){
        if(!isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Veuillez vous connecter', 'route' => 'connexion'));
        }

        $view = new Profil();

        $env = $this->app->environment();
        $view->addVar('path', $env['SCRIPT_NAME']);
        $view->addVar('link',links());
        $view->addVar('titre', 'Profil');
        $view->addVar('session', $_SESSION);
        echo $view->render();
    }

    public function editer(){
        if(!isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Veuillez vous connecter', 'route' => 'connexion'));
        }

        $token = generateToken();
        $view = new EditerProfil();

        $membre = Membre::find($_SESSION['id']);
        $env = $this->app->environment();
        $view->addVar('path', $env['SCRIPT_NAME']);
        $view->addVar('link',links());
        $view->addVar('membre', $membre);
        $view->addVar('token', $token);
        $view->addVar('titre', 'Editer mes informations');
        $view->addVar('session', $_SESSION);
        echo $view->render();
    }

    public function editerPost() {

        if(!isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Veuillez vous connecter', 'route' => 'connexion'));
        }

        if($_SESSION['token'] != $_POST['token']) {
            $this->app->error(array('messsage' => 'Clé d\'identification invalide', 'route' => 'editerProfil'));
        }

        $errors = array();
        $membre = Membre::find($_SESSION['id']);

        try {
            Authentication::authenticate($membre->email, $_POST['mot_de_passe']);
        } catch(AuthException $e) {
            $this->app->error(array('message' => 'Mot de passe incorrect', 'route' => 'editerProfil'));
        }

        MembreController::sanitizeNom($_POST['nom'], $membre, $errors);
        MembreController::sanitizePrenom($_POST['prenom'], $membre, $errors);
        MembreController::sanitizeMail($_POST['email'], $membre, $errors);
        MembreController::sanitizeCodePostal($_POST['code_postal'], $membre, $errors);
        MembreController::sanitizeTelephone($_POST['telephone'], $membre, $errors);

        if(sizeof($errors) == 0) {
            $membre->save();
        }
        if(sizeof($errors) > 0) {
            foreach ($errors as $key => $error) {
                $this->app->flash($key, $error);
            }
            $this->app->redirect($this->app->urlFor('editerProfil'));
        }

        Authentication::loadProfile($membre->email);
        $this->app->flash('success', 'Modifications enregistrées');
        $this->app->redirect($this->app->urlFor('profil'));

    }

    public function motDePasse() {
        if(!isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Veuillez vous connecter', 'route' => 'connexion'));
        }

        $token = generateToken();
        $view = new EditerMotDePasse();

        $membre = Membre::find($_SESSION['id']);
        $env = $this->app->environment();
        $view->addVar('path', $env['SCRIPT_NAME']);
        $view->addVar('link',links());
        $view->addVar('membre', $membre);
        $view->addVar('token', $token);
        $view->addVar('titre', 'Editer mes informations');
        $view->addVar('session', $_SESSION);
        echo $view->render();
    }

    public function motDePassePost() {

        if(!isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Veuillez vous connecter', 'route' => 'connexion'));
        }

        if($_SESSION['token'] != $_POST['token']) {
            $this->app->error(array('message' => 'Clé d\'identification invalide', 'route' => 'editerProfil'));
        }

        $errors = array();
        $membre = Membre::find($_SESSION['id']);

        try {
            Authentication::authenticate($membre->email, $_POST['mot_de_passe']);
        } catch(AuthException $e) {
            $this->app->error(array('message' => 'Mot de passe incorrect', 'route' => 'editerMotDePasse'));
        }

        MembreController::sanitizeMotDePasse($_POST['nouveau_mot_de_passe'], $membre, $errors);

        if(sizeof($errors) == 0) {
            $membre->mot_passe = password_hash($_POST['nouveau_mot_de_passe'], PASSWORD_DEFAULT, array('cost'=> 12));
            $membre->save();
        }
        if(sizeof($errors) > 0) {
            foreach ($errors as $key => $error) {
                $this->app->flash($key, $error);
            }
            $this->app->redirect($this->app->urlFor('editerMotDePasse'));
        }

        Authentication::loadProfile($membre->email);
        $this->app->flash('success', 'Modifications enregistrées');
        $this->app->redirect($this->app->urlFor('profil'));

    }

    public function api() {
        if(!isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Veuillez vous connecter', 'route' => 'connexion'));
        }

        $token = generateToken();
        $view = new GenererAPI();

        $membre = Membre::find($_SESSION['id']);
        $env = $this->app->environment();
        $view->addVar('path', $env['SCRIPT_NAME']);
        $view->addVar('link',links());
        $view->addVar('membre', $membre);
        $view->addVar('token', $token);
        $view->addVar('titre', 'Générer clé d\'API');
        $view->addVar('session', $_SESSION);
        echo $view->render();

    }

    public function apiPost(){
        if(!isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Veuillez vous connecter', 'route' => 'connexion'));
        }

        if($_SESSION['token'] != $_POST['token']) {
            $this->app->error(array('message' => 'Clé d\'identification invalide', 'route' => 'editerProfil'));
        }

        $membre = Membre::find($_SESSION['id']);
        $membre->api_key = strtoupper(generateToken());
        $membre->save();

        $this->app->flash('success', 'Clé d\'API générée avec succès');
        $this->app->redirect($this->app->urlFor('genererAPI'));

    }

    public function connexion() {

        if(isset($_SESSION['connecte'])) {
            $this->app->error("Vous êtes déjà connecté");
        }

        $token = generateToken();

        $view = new Connexion();
        $view->addVar('link',links());
        $env = $this->app->environment();
        $view->addVar('session', $_SESSION);
        $view->addVar('titre', 'Connexion');
        $view->addVar('token', $token);
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function connexionPost() {
        if($_SESSION['token'] != $_POST['token']) {
            $this->app->error(array('message' => 'Clé d\'identification invalide', 'route' => 'connexion'));
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        try {
            Authentication::authenticate($email, $password);
            Authentication::loadProfile($email);
        } catch(AuthException $e) {
            $this->app->error(array('message' => 'Identifiants invalides', 'route' => 'connexion'));
        }
        $this->app->flash('success', 'Connexion réussie');
        $this->app->redirect($this->app->urlFor('index'));
    }

    public function inscription() {
        if(isset($_SESSION['connecte'])) {
            $this->app->error("Vous êtes déjà connecté");
        }

        $token = generateToken();

        $view = new Inscription();
        $view->addVar('link',links());
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        $view->addVar('session', $_SESSION);
        $view->addVar('token', $token);
        $view->addVar('titre', 'Inscription');
        echo $view->render();
    }

    public function inscriptionPost() {

        if($_SESSION['token'] != $_POST['token']) {
            $this->app->error(array('message' => 'Clé d\'identification invalide', 'route' => 'inscription'));
        }

        $res = $this->app->request->post();
        $errors = array();
        $membre = new Membre();

        MembreController::sanitize($res, $membre, $errors);

        if(sizeof($errors) == 0) {
            $membre->mot_passe = password_hash($res['mot_de_passe'], PASSWORD_DEFAULT, array('cost'=> 12));
            $membre->save();
        }
        if(sizeof($errors) > 0) {
            foreach ($errors as $key => $error) {
                $this->app->flash($key, $error);
            }
            $this->app->redirect($this->app->urlFor('inscription'));
        }

        Authentication::loadProfile($membre->email);

        $this->app->flash('success', 'Inscription réussie');
        $this->app->redirect($this->app->urlFor('index'));
    }

    public function deconnexion() {
        session_unset();
        session_destroy();
        session_start();
        $this->app->flash('success', 'Déconnexion réussie');
        $this->app->redirect($this->app->urlFor('index'));
    }

    //Fonction utiliser pour nettoyer et définir les données en entrée d'un ajout ou d'un update de membre
    public static function sanitize($data, $membre, &$errors) {
        if(array_key_exists('nom', $data)) {
            MembreController::sanitizeNom($data['nom'], $membre, $errors);
        } else {
            $errors['nom'] = "Champ nom non défini";
        }
        if(array_key_exists('prenom', $data)) {
            MembreController::sanitizePrenom($data['prenom'], $membre, $errors);
        } else {
            $errors['prenom'] = "Champ prenom non défini";
        }
        if(array_key_exists('email', $data)) {
            MembreController::sanitizeMail($data['email'], $membre, $errors);
        } else {
            $errors['mail'] = "Champ email non défini";
        }
        if(array_key_exists('telephone', $data)) {
            MembreController::sanitizeTelephone($data['telephone'], $membre, $errors);
        } else {
            $errors['tel'] = "Champ telephone non défini";
        }
        if(array_key_exists('code_postal', $data)) {
            MembreController::sanitizeCodePostal($data['code_postal'], $membre, $errors);
        } else {
            $errors['code_postal'] = "Champ code_postal non défini";
        }
        if(array_key_exists('mot_de_passe', $data)) {
            MembreController::sanitizeMotDePasse($data['mot_de_passe'], $membre, $errors);
        } else {
            $errors['mot_de_passe'] = "Champ mot_de_passe non défini";
        }
    }

    public static function sanitizeNom($value, $membre, &$errors) {
        if(strlen($value) < 3 || strlen($value) > 70) {
            $errors['nom'] = "Erreur sur le champ nom (entre 3 et 70 caractères)";
        } else {
            $membre->nom = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
        }
    }

    public static function sanitizePrenom($value, $membre, &$errors) {
        if(strlen($value) < 3 || strlen($value) > 70) {
            $errors['prenom'] = "Erreur sur le champ prénom (entre 3 et 70 caractères)";
        } else {
            $membre->prenom = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
        }
    }

    public static function sanitizeMail($value, $membre, &$errors) {
        if(strlen($value) < 3 || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = "Erreur sur le champ mail (3 caractères minimum et email valide obligatoire)";
        } else {
            $membre->email = $value;
        }
    }

    public static function sanitizeTelephone($value, $membre, &$errors) {
        $telRegex = array("options"=>array("regexp"=>"/^\+?([\s-\(\.]*[0-9][\s\)]*){10,}$/"));

        if(strlen($value) < 10 || strlen($value) > 20 || !filter_var($value, FILTER_VALIDATE_REGEXP, $telRegex)) {
            $errors['tel'] = "Erreur sur le champ téléphone (entre 10 et 20 caractères)";
        } else {
            $membre->telephone = $value;
        }
    }

    public static function sanitizeCodePostal($value, $membre, &$errors) {
        if(strlen($value) < 5 || strlen($value) > 5) {
            $errors['code_postal'] = "Erreur sur le code postal (5 caractères seulement)";
        } else {
            $membre->code_postal = $value;
        }
    }

    public static function sanitizeMotDePasse($value, $membre, &$errors) {
        if(strlen($value) < 5 || strlen($value) > 15) {
            $errors['password'] = "Erreur sur le champ mot de passe (entre 5 et 15 caractères)";
        } else {
            $membre->mot_passe = $value;
        }
    }

    public static function membreDepuisSession($membre) {
        $membre->id = $_SESSION['id'];
        $membre->nom = $_SESSION['nom'];
        $membre->prenom = $_SESSION['prenom'];
        $membre->email = $_SESSION['email'];
        $membre->telephone = $_SESSION['telephone'];
        $membre->code_postal = $_SESSION['code_postal'];
    }

    public function mesAnnonces() {

        $res = $this->app->request->get();

        $annonces = null;
        if(array_key_exists('id', $_SESSION)) {
            $annonces = Annonce::where('id_membre', '=', $_SESSION['id'])->orderBy('date_creation', 'desc')->with('images')->get();
        } else if(array_key_exists('email', $res)) {
            try {
                $membre = Membre::where('email', '=', $res['email'])->firstOrFail();
            } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                $this->app->error(array('message' => 'Aucune annonce ne correspond à cet email', 'route' => 'mesAnnonces'));
            }
            $annonces = $membre->ad()->with('images')->get();
        }

        $view = new ListeAnnonces();

        if($annonces == null && !array_key_exists('email', $res)) {
            $view->addVar('recherche', true);
        } else if (sizeof($annonces) == 0 && isset($_SESSION['id'])) {
            $this->app->error(array('message' => 'Vous n\'avez pas encore d\'annonces', 'route' => 'index'));
        } else if (sizeof($annonces) == 0) {
            $this->app->error(array('message' => 'Aucune annonce ne correspond à cet email', 'route' => 'mesAnnonces'));
        } else {
            foreach ($annonces as $annonce) {
                $annonce['url'] = $this->app->urlFor('annonce',array('id' => $annonce['id']) );
                $annonce['image'] = $annonce->cover();
            }
        }

        $view->addVar('link',links());
        $view->addVar('annonces', $annonces);
        $view->addVar('url', $annonces);
        $view->addVar('titre', "Mes annonces");
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        $view->addVar('session', $_SESSION);
        echo $view->render();
    }

}