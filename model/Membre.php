<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Membre extends Model{

    public $table = 'membre';
    public $idTable = 'id';
    public $timestamps = false;

    /*$public function address() {
        return $this->belongsTo('Adresse', 'id_adresse');
    }*/

    public function ad() {
        return $this->hasMany('model\Annonce', 'id_membre');
    }

}